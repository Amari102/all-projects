#!/bin/sh

if ! command -v black ; then
    >&2 echo "black is not installed, skipping."
else
    black "$PWD"
fi

if ! command -v isort ; then
    >&2 echo "isort is not installed, skipping."
else
    isort "$PWD" --profile=attrs
fi
