#!/bin/bash
set -e

REPO_NAME='dvws-node'
REPO_URL='https://github.com/snoopysecurity/dvws-node.git'

if [ ! -d $REPO_NAME ]; then
    git clone $REPO_URL
fi

cd $REPO_NAME
