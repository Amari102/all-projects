This is a monorepo containing most of my projects. Highlights:

### <img src="./rhasspy_intent_handler_asyncio/rhasspy_intent_handler_logo16x.png" alt="logo"/> Rhasspy Intent Handler

*(./rhasspy_intent_handler_asyncio)*

Asyncio implementation of intent handler for [Rhasspy Voice Assistant](https://rhasspy.readthedocs.io/en/latest/).

### Asyncio examples
*(./asyncio_examples)*

Examples showcasing usage of Python [asyncio](https://docs.python.org/3/library/asyncio.html) library.

### Python code examples
*(./python_code_examples)*

Snippets illustrating solutions to various programming problems.

### API Testing Challenges
*./api_challenges*

Solutions to the API Challenges project created by Alan Richardson https://www.eviltester.com/page/tools/apichallenges/
