#!/bin/bash

API_CHALLENGES_SERVER_BINARY="apichallenges.jar"

if [ ! -f $API_CHALLENGES_SERVER_BINARY ]; then
    echo "Download $API_CHALLENGES_SERVER_BINARY from" \
    "https://www.eviltester.com/page/tools/apichallenges/" \
    "and put it in this directory."
    exit 1
fi

java -jar $API_CHALLENGES_SERVER_BINARY
