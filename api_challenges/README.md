# Solutions to the API Testing Challenges
`$SERVER` refers to the instance of API Challenges server.

## Solutions
----
### Challenge 04
> Issue a GET request on the `/challenges` end point

* `json.tool` pretty prints the resulting JSON
* `bat` command colorizes the output and adds pager.
* `jq` is a JSON processor that can extract selected fields.
```bash
curl $SERVER/challenges | python -m json.tool | bat -l json
curl $SERVER/challenges | python -m json.tool | jq -r '.challenges[] | "\n", .id, .description '
curl $SERVER/challenges | python -m json.tool | jq '.challenges[] | select(.id == "03") | {id, description} '
```
----
### 03
> Issue a GET request on the `/todos` end point

```bash
url -s $SERVER/todos | python -m json.tool | bat -l json
```
----
### 04
> Issue a GET request on the `/todo` end point should 404 because nouns should be plural

Extract only response code from the curl output:
```bash
curl -s -o /dev/null -w '%{http_code}' $SERVER/todo
```
Search for expected 404, using `ripgrep`:
```bash
curl -s -o /dev/null -w '%{http_code}' $SERVER/todo | rg 404
```

----
### 05
> Issue a GET request on the `/todos/{id}` end point to return a specific todo

```bash
curl -s $SERVER/todos/2 | python -m json.tool | bat -l json
```
----
### 06
> Issue a GET request on the `/todos/{id}` end point for a todo that does not exist

```bash
curl -s $SERVER/todos/foo | python -m json.tool | bat -l
```
----
### 07
> Issue a HEAD request on the `/todos` end point

```bash
curl --head $SERVER/todos | bat
```
----
### 08
> Issue a POST request to successfully create a todo

```bash
curl -X POST -H "Content-Type: application/json" -d "{"title": "do something later"}" $SERVER/todos
```

----
### 09
> Issue a GET request on the `/todos` end point with a query filter to get only todos which are 'done'. There must exist both 'done' and 'not done' todos, to pass this challenge.

Update todo item status:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"title": "schedule meeting", "doneStatus": true}' $SERVER/todos
```

Send query with filter:
```bash
curl -s $SERVER/todos\?doneStatus=true | python -m json.tool | bat -l json
```
----
### 10
> Issue a POST request to create a todo but fail validation on the `doneStatus` field

I tried both updating and creating new record:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"title": "schedule meeting", "doneStatus": foo}' $SERVER/todos
curl -X POST -H "Content-Type: application/json" -d '{"title": "schedule meeting 2", "doneStatus": foo}' $SERVER/todos
```
----
### 11
> Issue a POST request to successfully update a todo

Done in challenge #09.

----
### 12
> Issue a DELETE request to successfully delete a todo

```bash
# curl -X DELETE $SERVER/tbind \cp copy-selectionodos/11
```

----
### 13
> Issue an OPTIONS request on the `/todos` end point. You might want to manually check the 'Allow' header in the response is as expected.

`-i` includes response header.
```bash
curl -X OPTIONS -i -H "Allow: OPTIONS,GET,POST,HEAD" $SERVER/todos
curl --head $SERVER/todos | bat
```

----
### 14
> Issue a GET request on the `/todos` end point with an `Accept` header of `application/xml` to receive results in XML format

```bash
curl -H "Accept: application/xml" $SERVER/todos
```

----
### 15
> Issue a GET request on the `/todos` end point with an `Accept` header of `application/json` to receive results in JSON format

```bash
curl -H "Accept: application/json" $SERVER/todos
```

----
### 16
> Issue a GET request on the `/todos` end point with an `Accept` header of `*/*` to receive results in default JSON format

```bash
curl -H "Accept: */*" $SERVER/todos

```

----
### 17
> Issue a GET request on the `/todos` end point with an `Accept` header of `application/xml, application/json` to receive results in the preferred XML format

```bash
curl -H "Accept: application/xml, application/json" $SERVER/todos
```

----
### 18
> Issue a GET request on the `/todos` end point with no `Accept` header present in the message to receive results in default JSON format

```bash
curl -H "Accept: application/json, application/xml" $SERVER/todos
```

----
### 19
> Issue a GET request on the `/todos` end point with an `Accept` header `application/gzip` to receive 406 'NOT ACCEPTABLE' status code

```bash
curl -H "Accept: application/gzip" -w "HTTP code was %{http_code}" $SERVER/todos
```

----
### 20
> Issue a POST request on the `/todos` end point to create a todo using Content-Type `application/xml`, and Accepting only XML ie. Accept header of `application/xml`

```bash
curl -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" -d '<todo><title>todo xml test</title><doneStatus>false</doneStatus></todo>' $SERVER/todos
```

----
### 21
> Issue a POST request on the `/todos` end point to create a todo using Content-Type `application/json`, and Accepting only JSON ie. Accept header of `application/json`

```bash
curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"title": "todo challenge no 21", "doneStatus": true}' $SERVER/todos
```


----
### 22
> Issue a POST request on the `/todos` end point with an unsupported content type to generate a 415 status code

```bash
curl -X POST -H "Content-Type: application/foo" -H "Accept: application/json" -d 'content with unsupported type' $SERVER/todos
```

----
### 23
> Issue a POST request on the `/todos` end point to create a todo using Content-Type `application/xml` but Accept `application/json`

```bash
curl -X POST -H "Content-Type: application/xml" -H "Accept: application/json" -d '<todo><title>todo xml test</title><doneStatus>false</doneStatus></todo>' $SERVER/todos
```

----
### 24
> Issue a POST request on the `/todos` end point to create a todo using Content-Type `application/json` but Accept `application/xml`

```bash
curl -X POST -H "Content-Type: application/json" -H "Accept: application/xml" -d '{"title": "todo challenge no 21", "doneStatus": true}' $SERVER/todos
```

----
### 25
> Issue a DELETE request on the `/heartbeat` end point and receive 405 (Method Not Allowed)

```bash
curl -X DELETE $SERVER/heartbeat -i
```

### 26
> Issue a PATCH request on the `/heartbeat` end point and receive 500 (internal server error)

```bash
curl -X PATCH $SERVER/heartbeat -i
```

----
### 27
> Issue a TRACE request on the `/heartbeat` end point and receive 501 (Not Implemented)

```bash
curl -X TRACE $SERVER/heartbeat -i
```

----
### 28
> Issue a GET request on the `/heartbeat` end point and receive 204 when server is running

```bash
curl $SERVER/heartbeat -i
```

----
### 29
> Issue a POST request on the `/secret/token` end point and receive 401 when Basic auth username/password is not admin/password

Basic authorization is string username:password encoded in base64.
```bash
curl -X POST -H "Authorization: Basic ZHVwYQ==" $SERVER/secret/token -i
```

----
### 30
> Issue a POST request on the `/secret/token` end point and receive 201 when Basic auth username/password is admin/password

It's important to strip trailing newline with `echo -n` before converting auth info to base64.
```bash
curl -X POST -H "Authorization: Basic $(echo -n 'admin:password' | base64)" $SERVER/secret/token -i
```
Curl also provides option to automatically create authorization header:
```bash
curl -X POST -u admin:password $SERVER/secret/token -i
```

----
### 31
> Issue a GET request on the `/secret/note` end point and receive 403 when X-AUTH-TOKEN does not match a valid token

```bash
curl -X POST -H "X-Auth-Token: foo" -u admin:password $SERVER/secret/note -i
```

----
### 32
> Issue a GET request on the `/secret/note` end point and receive 401 when no X-AUTH-TOKEN header present

```bash
curl $SERVER/secret/note -i
```

----
### 33
> Issue a GET request on the `/secret/note` end point receive 200 when valid X-AUTH-TOKEN used - response body should contain the note

X-Auth-Token was created when posting request to /secret/token endpoint.
```bash
curl -H "X-Auth-Token: 28af4891-d09a-4f55-af36-f565d78a79c5" $SERVER/secret/note -i
```
<details>

<summary>Results</summary>

```
HTTP/1.1 200 OK
Date: Sat, 04 Feb 2023 18:40:37 GMT
Content-Type: application/json
ACCESS-CONTROL-ALLOW-ORIGIN: *
ACCESS-CONTROL-ALLOW-HEADERS: *
X-CHALLENGER: rest-api-challenges-single-player
Transfer-Encoding: chunked
Server: Jetty(9.4.z-SNAPSHOT)

{"note":""}⏎
```
</details>

----
### 34
> Issue a POST request on the `/secret/note` end point with a note payload e.g. {"note":"my note"} and receive 200 when valid X-AUTH-TOKEN used. Note is maximum length 100 chars and will be truncated when stored.

```bash
curl -X POST -H "X-Auth-Token: 28af4891-d09a-4f55-af36-f565d78a79c5" -H "Content-Type: application/json" -d '{"note": "my note"}' $SERVER/secret/note -i
```
<details>

<summary>Results</summary>

```
HTTP/1.1 200 OK
Date: Sat, 04 Feb 2023 18:49:27 GMT
X-CHALLENGER: rest-api-challenges-single-player
Content-Type: application/json
ACCESS-CONTROL-ALLOW-ORIGIN: *
ACCESS-CONTROL-ALLOW-HEADERS: *
Transfer-Encoding: chunked
Server: Jetty(9.4.z-SNAPSHOT)

{"note":"my note"}⏎
```
</details>

----
### 35
> Issue a POST request on the `/secret/note` end point with a note payload {"note":"my note"} and receive 401 when no X-AUTH-TOKEN present

```bash
curl -X POST -H "Content-Type: application/json" -d '{"note": "my note"}' $SERVER/secret/note -i
```
<details>

<summary>Results</summary>

```
HTTP/1.1 401 Unauthorized
Date: Sat, 04 Feb 2023 18:54:23 GMT
X-CHALLENGER: rest-api-challenges-single-player
Content-Type: application/json
ACCESS-CONTROL-ALLOW-ORIGIN: *
ACCESS-CONTROL-ALLOW-HEADERS: *
Transfer-Encoding: chunked
Server: Jetty(9.4.z-SNAPSHOT)
```
</details>

----
### 36
> Issue a POST request on the `/secret/note` end point with a note payload {"note":"my note"} and receive 403 when X-AUTH-TOKEN does not match a valid token

```bash
curl -X POST -H "X-Auth-Token: foo" -H "Content-Type: application/json" -d '{"note": "my note"}' $SERVER/secret/note -i
```
<details>

<summary>Results</summary>

```
HTTP/1.1 403 Forbidden
Date: Sat, 04 Feb 2023 18:55:46 GMT
X-CHALLENGER: rest-api-challenges-single-player
Content-Type: application/json
ACCESS-CONTROL-ALLOW-ORIGIN: *
ACCESS-CONTROL-ALLOW-HEADERS: *
Transfer-Encoding: chunked
Server: Jetty(9.4.z-SNAPSHOT)

```
</details>

----
### 37
> Issue a GET request on the `/secret/note` end point receive 200 when using the X-AUTH-TOKEN value as an Authorization Bearer token - response body should contain the note

The value for Bearer field is X-Auth-Token retrieved in challenge #30.
```bash
curl -H "Authorization: Bearer 28af4891-d09a-4f55-af36-f565d78a79c5" $SERVER/secret/note -i
```
<details>

<summary>Results</summary>

```
HTTP/1.1 200 OK
Date: Sat, 04 Feb 2023 19:14:14 GMT
Content-Type: application/json
ACCESS-CONTROL-ALLOW-ORIGIN: *
ACCESS-CONTROL-ALLOW-HEADERS: *
X-CHALLENGER: rest-api-challenges-single-player
Transfer-Encoding: chunked
Server: Jetty(9.4.z-SNAPSHOT)

{"note":"my note"}
```
</details>

----
### 38
> Issue a POST request on the `/secret/note` end point with a note payload e.g. {"note":"my note"} and receive 200 when valid X-AUTH-TOKEN value used as an Authorization Bearer token. Status code 200 received. Note is maximum length 100 chars and will be truncated when stored.


```bash
curl -X POST -H "Authorization: Bearer 28af4891-d09a-4f55-af36-f565d78a79c5" -H "Content-Type: application/json" -d '{"note": "my note"}' $SERVER/secret/note -i
```
<details>

<summary>Results</summary>

```
HTTP/1.1 200 OK
Date: Sat, 04 Feb 2023 19:17:59 GMT
X-CHALLENGER: rest-api-challenges-single-player
Content-Type: application/json
ACCESS-CONTROL-ALLOW-ORIGIN: *
ACCESS-CONTROL-ALLOW-HEADERS: *
Transfer-Encoding: chunked
Server: Jetty(9.4.z-SNAPSHOT)

{"note":"my note"}
```
</details>

----
### 39
> Issue a DELETE request to successfully delete the last todo in system so that there are no more todos in the system


```bash
curl -s -X DELETE $SERVER/todos/1 -i
```
----

### Miscellaneous:
Code snippet to download the challenge description and copy it to X11 clipboard:
```bash
curl -s $SERVER/challenges | python -m json.tool | jq -r '.challenges[] | select(.id == "03") | .description ' | xclip -sel c
```

### Resources
Curl documentation: https://curl.se/docs/manpage.html
API Challenges website: https://www.eviltester.com/page/tools/apichallenges/
