import qualified Data.Text as Txt

stringStartsWithSubstring1 :: String -> String -> Bool
stringStartsWithSubstring1 str subStr = take (length subStr) str == subStr
