-- Define reverse, which reverses a list, using foldr.
freverse1 :: [a] -> [a]
freverse1 xs = foldl (\acc x -> x : acc) [] xs

freverse2 :: [a] -> [a]
freverse2 = foldl (flip (:)) []
-- [1,2,3,4,5]
-- flip (:) on [] 1 results in 1 : [] = [1]
-- flip (:) on [1] 2 results in 2 : [1] = [2,1]
-- flip (:) on [2,1] 3 results in 3 : [2,1] = [3,2,1]
-- ...
