generatePi1 :: Int -> Double
generatePi1 n
    | n <= 0 = error("First argument muse be a positive integer")
    | n == 1 = read ([head (show pi)]) :: Double
    | n >= 17 = error ("Too many digits requested")
    | n > 1 = read (take (n+1) (show pi)) :: Double


generatePi2 :: Int -> Double
generatePi2 n
    | n <= 0 = error("First argument muse be a positive integer")
    | n == 1 = readAsDouble [head (piAsString)]
    | n >= 17 = error ("Too many digits requested")
    | n = readAsDouble (take (n+1) (piAsString))
    where readAsDouble x = read x :: Double
          piAsString = show pi

