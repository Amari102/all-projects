-- Using foldr , define a function remove which takes two strings as its arguments and removes every letter from the second list that occurs in the first list. For example, remove "first" "second" = "econd"
remover1 :: String -> String -> String
remover1 first' second = foldr (\x acc -> if elem x first' then acc else x : acc ) [] second

remover2 :: String -> String -> String
remover2 first' = foldr (\x acc -> if x `elem` first' then acc else x : acc ) []
