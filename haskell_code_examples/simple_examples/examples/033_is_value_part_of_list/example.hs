isVal1 :: (Eq a) => a -> [a] -> Bool
isVal1 = elem

isVal2 :: (Eq a) => a -> [a] -> Bool
isVal2 x xs = foldl (\a x -> if x == a then True else a ) False xs
