-- using the higher-order function foldr define a function sumsq which takes an
-- integer n as its argument and returns the sum of the squares of the first n
-- integers
solution1 :: (Integral a) => a -> a
solution1 n = sum (map (^2) [1..n])

solution2 :: (Integral a) => a -> a
solution2 n = sum [ x^2 | x <- [1..n] ]

solution3 :: (Integral a) => a -> a
solution3 n = sum (map (\x -> x^2) [1..n])

solution4 :: (Integral a) => a -> a
solution4 n = foldl func 0 [1..n]
    where func = (\acc x -> acc + x^2)

-- foldr has accumulator on the right side.
solution5 :: (Integral a) => a -> a
solution5 n = foldr func 0 [1..n]
    where func = (\x acc -> acc + x^2)
