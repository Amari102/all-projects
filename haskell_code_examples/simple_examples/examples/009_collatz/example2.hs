import Test.QuickCheck

prop_reverse :: [Int] -> Bool
prop_reverse xs = reverse (reverse xs) == xs

prop_collatz :: Positive Int -> Bool
prop_collatz (Positive x) = last (hailstoneSequence [x]) == 1

hailstoneSequenceReversed :: [Int] -> [Int]
hailstoneSequenceReversed foo
    | firstElement == 1 = foo
    | even firstElement = hailstoneSequenceReversed (hailstone firstElement : foo)
    | odd firstElement = hailstoneSequenceReversed (hailstone firstElement : foo)
    where firstElement = head foo

hailstoneSequence :: [Int] -> [Int]
hailstoneSequence xs = reverse (hailstoneSequenceReversed xs)

hailstone :: Int -> Int
hailstone n
    | n <= 0 = error "Only positive integers are allowed as input"
    | even n = div n 2
    | odd n = (3*n)+1

main :: IO ()
main = do
    putStrLn "Please enter a positive integer number."
    line <- getLine
    let num = read line :: Int
    putStrLn (show (hailstoneSequence [num]))

    -- quickCheck prop_collatz
    verboseCheck prop_collatz
