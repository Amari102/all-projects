collatz :: Integer -> [Integer]
collatz x
    | x <= 0 = error "Number must be positive"
    | x == 1 = []
    | odd x =  cOdd: collatz cOdd
    | even x = cEven : collatz cEven
    where
        cOdd = (3 * x +1)
        cEven = (div x 2)

-- for all starting numbers between 1 and 100, how many chains
-- have a length greater than 15?

answer1 :: Integer
answer1 = length (filter (>15) (map length (map collatz [1,2..100])))

answer2 :: Integer
answer2 = length (filter ((> 15) . length) ( map collatz [1,2..100] ))

answer3 :: Integer
answer3 = length (filter (\x -> length x > 15) ( map collatz [1,2..100] ))

answer4 :: Integer
answer4 = length [length (collatz x) | x <- [1,2..100], length (collatz x) > 15]
