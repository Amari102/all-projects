flip1 :: (a -> b -> c) -> (b - > a -> c)
flip1 f = g
	where g x y = f y x

flip2 :: (a -> b -> c) -> (b - > a -> c)
flip2 f = \x y -> f y x
