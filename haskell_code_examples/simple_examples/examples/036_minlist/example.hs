-- Define minlist, which returns the smallest integer in a non-empty list of integers, using foldr1. Redefine it using foldl1.
minlist1 :: (Ord a) => [a] -> a
minlist1 xs = foldl1 (\acc x -> if (x < acc) then x else acc) xs

minlist2 :: (Ord a) => [a] -> a
minlist2 xs = foldl1 (\x acc -> if (x < acc) then x else acc) xs
