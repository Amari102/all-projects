lastElement1 :: [a] -> a
lastElement1 x = last x

lastElement2 :: [a] -> a
lastElement2 = last

lastElement3 :: [a] -> a
lastElement [] = error "There is no last element in empty list"
lastElement3 [x] = x
lastElement3 (x:xs) = lastElement3 xs
