-- find the largest number under 100 000 that's divisible by 3829

finder1 :: Int
finder1 = head [ a | a <- [100000,99999..0], mod a 3829 == 0]

finder2 :: Int
finder2 = head (filter f [100000,99999..0])
    where f x = mod x 3829 == 0
