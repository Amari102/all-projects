-- Define length, which returns the number of elements in a list, using foldr . Redefine it using foldl.
len1 :: (Num a) => [a] -> a
len1 xs = func 0 xs
    where func = foldl (\acc x -> acc + 1)

len2 :: (Num a) => [a] -> a
len2 xs = func 0 xs
    where func = foldr (\x acc -> acc + 1)
