quickSort1 :: (Ord a) => [a] -> [a]
quickSort1 [] = []
quickSort1 (x:xs) =
    smallerSorted ++ [x] ++ biggerSorted
    where
        smallerSorted = quickSort1 [a | a <- xs, a <= x]
        biggerSorted = quickSort1 [a | a <- xs, a > x]

quicksort2 :: (Ord a) => [a] -> [a]
quicksort2 [] = []
quicksort2 (x:xs) =
    let smallerSorted = quicksort2 (filter (<=x) xs)
        biggerSorted = quicksort2 (filter (>x) xs)
    in  smallerSorted ++ [x] ++ biggerSorted


quicksort3 :: (Ord a) => [a] -> [a]
quicksort3 [] = []
quicksort3 x = x
quicksort3 (x:xs) = smallerSorted ++ [x] ++ biggerSorted
    where
        smallerSorted = quicksort3 (filter (<=x) xs)
        biggerSorted = quicksort3 (filter(>x) xs)
