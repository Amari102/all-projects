fizzbuzz :: [Int] -> [String]
fizzbuzz xs = [ fizzbuzzElement x | x <- xs]

fizzbuzzElement :: Int -> String
fizzbuzzElement x
    | mod x 3 == 0 && mod x 5 == 0 = "FizzBuzz"
    | mod x 3 == 0 = "Fizz"
    | mod x 5 == 0 = "Buzz"
    | otherwise = show x

main :: IO ()
main = print (fizzbuzz [1,2..100])
