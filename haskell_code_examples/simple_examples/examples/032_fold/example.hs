factorial1 :: (Num a) => [a] -> a
factorial1 xs = foldl (\x y -> x * y ) 1 xs

factorial2 :: (Num a) => [a] -> a
factorial2 = foldl (\x y -> x * y ) 1

-- ghci> :t foldl
-- foldl :: Foldable t => (b -> a -> b) -> b -> t a -> b
-- ghci> :t (*)
-- (*) :: Num a => a -> a -> a
-- ghci> :t (\x y -> x * y )
-- (\x y -> x * y ) :: Num a => a -> a -> a
factorial3 :: (Num a) => [a] -> a
factorial3 = foldl (*) 1

