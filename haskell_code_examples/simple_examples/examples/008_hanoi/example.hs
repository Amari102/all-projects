type Peg = String
type Move = (Peg, Peg)
type DiskCount = Integer


hanoi4 :: DiskCount -> Peg -> Peg -> Peg -> Peg -> [Move]
-- not the most optimal solution, but more efficient than hanoi3
--   █     |   |   |
--  ███    |   |   |
-- █████   |   |   |
-- first argument is the original hanoi stack position.
-- second argument is the destination hanoi stack position.
-- third argument is the peg which will be used as a temporary storage.
hanoi4 0 orig dest tmp1 tmp2 = []
hanoi4 1 orig dest tmp1 tmp2 = [(orig, dest)]
hanoi4 n orig dest tmp1 tmp2 = (hanoi4 (n-2) orig tmp1 dest tmp2) ++ [(orig, tmp2)] ++ [(orig, dest)] ++  [(tmp2, dest)] ++ (hanoi4 (n-2) tmp1 dest orig tmp2)

-- this version of hanoi takes ~ 15 seconds to run on my workstation with 20 discs.
hanoi :: DiskCount -> Peg -> Peg -> Peg -> [Move]
--   █     |   |
--  ███    |   |
-- █████   |   |
-- first argument is the original hanoi stack position.
-- second argument is the destination hanoi stack position.
-- third argument is the peg which will be used as a temporary storage.
hanoi 0 orig dest tmp = []
hanoi n orig dest tmp = (hanoi (n-1) orig tmp dest) ++ [(orig, dest)] ++ (hanoi (n-1) tmp dest orig)

main :: IO ()
-- main = print (hanoi 5 "a" "b" "c")
main = print (length (hanoi4 5 "a" "b" "c" "d"))

