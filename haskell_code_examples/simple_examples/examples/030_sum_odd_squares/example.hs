-- find the sum of all odd squares that are smaller than 10,000

-- list evaluation never ends in this example
-- finder1 :: Integer
-- finder1 = sum allOddSquaresUnder10k
--     where allOddSquaresUnder10k = [ x | x <- [1,2..], x^2 < 10000 ]

finder1 :: Integer
finder1 = sum allOddSquaresUnder10k
    where allOddSquaresUnder10k = takeWhile (<10000) [ x^2 | x <- [1,2..], odd x ]

finder2 :: Integer
finder2 = sum allOddSquaresUnder10k
    where allOddSquaresUnder10k = takeWhile (<10000) (map (^2) (filter odd [1,2..]))
