## Description
Collection of simple examples meant to help me get familiarized with Haskell. The examples \
were inspired by the following sources:
- https://projecteuler.net/archives
- https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems
- https://github.com/karan/Projects
- https://programming-idioms.org/search/haskell

## Requirements
The only system package required to run this example is Nix package manager: \
https://nixos.org/download.html

To enter the nix shell:
```sh
nix-shell
```

## Used technologies
This example showcases the following technologies:
 - Nix package manager
   - Nix shell
 - Haskell
   - Stack

## Usage
Inside nix shell:

### Run GHCI interpreter
```sh
stack ghci
```

### Compile and run example file
```sh
stack ghc -- -o run_example example.hs
./ run_example
```

### Run example file
```sh
stack runghc example.hs
```

### Run example file in the interpreter
```sh
stack ghci
:l example.hs
```

#### Measure time of function execution
```sh
stack ghci
:load example.hs
:set +s
time_consuming_function param1 param2
```

## Troubleshooting
> bash: ./run.sh: Permission denied

Solution: make script executable:
```sh
chmod +x run.sh
```

## Examples list
- 001 - Find Pi to the n-th digit.
- 002 - Find last element of the list
- 003 - Take N last elements of a list
- 004 - Extract substring from a list
- 005 - Check if string starts with substring
- 006 - Quicksort
- 007 - FizzBuzz
- 008 - Hanoi solver
- 009 - Collatz conjecture.
	For all starting numbers between 1 and 100, how many chains have a length greater than 15?
- 010 - BMI calculator
- 011 - List reversal
- 012 - List flattener
- 013 - List duplicate remover
- 014 - List duplicates packer (packs duplicates into sublists)
- 015 - Count uppercase letters in a string
- 016 - Check if string is a palindrome.
- 017 - Split string based on string delimiter
- 018 - Count words in a file
- 019 - Print last N lines of file
- 020 - ROT13 converter
- 021 - Sumtorial
- 022 - Number guesser
- 023 - String repeater, prints phrase N times
- 024 - Return N-th element of list
- 025 - Finds first element of list of strings that has length of N
- 026 - zipWith function
- 027 - higher-order function that applies a parameter function twice
- 028 - higher-order function that applies a parameter function N times
- 029 - find the largest number under 100 000 that's divisible by 3829
- 030 - find the sum of all odd squares that are smaller than 10,000
- 031 - higher-order function that flips the parameters
- 032 - any function using fold
- 033 - check if value is part of list
- 034 - using the higher-order function foldr define a function sumsq which takes an integer n as its argument and returns the sum of the squares of the first n integers
- 035 - Define length, which returns the number of elements in a list, using foldr. Redefine it using foldl.
- 036 - Define minlist, which returns the smallest integer in a non-empty list of integers, using foldr1. Redefine it using foldl1.
- 037 - Define reverse, which reverses a list, using foldr.
- 038 - Using foldr , define a function remove which takes two strings as its arguments and removes every letter from the second list that occurs in the first list. For example, remove "first" "second" = "econd"
- 039 - Define filter using foldr . Define filter again using foldl.
- 040 - The function inits returns the list of all initial segments of a list. Thus, inits "ate" = [[], "a", "at", "ate"]. Define inits using foldr .
- 041 - Run-length encoded string. For example, if the input string is “wwwwaaadexxxxxx”, then the function should return “w4a3d1e1x6”

