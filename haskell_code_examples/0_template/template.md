## Description
A short description of the example.

## Requirements
Programs required to run this example and instructions how to set them up.

## Used technologies
This example showcases the following technologies:
 - Nix package manager
   - Nix shell
 - Technology 1
 - Technology 2
 - Technology 3

## Usage
Inside nix shell:

### Run GHCI interpreter
```sh
stack ghci
```

### Compile and run example file
```sh
stack ghc -- -o run_example example.hs
./ run_example
```

## Troubleshooting
> bash: ./run.sh: Permission denied

Solution: make script executable:
```sh
chmod +x run.sh
