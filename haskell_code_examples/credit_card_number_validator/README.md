## Description
Example of credit card number validator, from  \
https://www.seas.upenn.edu/~cis1940/spring13/hw/01-intro.pdf

Double the value of every second digit beginning from the right. That is, the last digit is unchanged; the second-to-last digit is doubled; the third-to-last digit is unchanged; and so on. For example, [1,3,8,6] becomes [2,3,16,6].

Add the digits of the doubled values and the undoubled digits from the original number. For example, [2,3,16,6] becomes 2+3+1+6+6 = 18.

Calculate the remainder when the sum is divided by 10. For the above example, the remainder would be 8. If the result equals 0, then the number is valid.


## Requirements
The only system package required to run this example is Nix package manager: \
https://nixos.org/download.html

To enter the nix shell:
```sh
nix-shell
```

## Used technologies
This example showcases the following technologies:
 - Nix package manager
   - Nix shell
 - Haskell
   - Stack

## Usage
Inside nix shell:

### Run GHCI interpreter
```sh
stack ghci
```

### Compile and run example file
```sh
stack ghc -- -o run_example example.hs
./ run_example
```

## Troubleshooting
> bash: ./run.sh: Permission denied

Solution: make script executable:
```sh
chmod +x run.sh
```
