import Data.Char

doubleUp :: [Int] -> [Int]
-- Doubles every second list element, starting from the end.
doubleUp (x:y:xs)
    | even (length (x:y:xs)) = 2*x : y : doubleUp xs
    | odd (length (x:y:xs)) = x : 2*y : doubleUp xs
doubleUp x = x


integerListToDigits :: [Int] -> [Int]
-- Split list of integers into separate digits. Example: [12,34] -> [1,2,3,4]
integerListToDigits [] = []
integerListToDigits (x:xs) = integerToDigits x ++ integerListToDigits xs

integerToDigits :: Int -> [Int]
-- Convert integer into digits. Example: 1234 -> [1,2,3,4]
integerToDigits n = [digitToInt x | x <- show n]

mod10Validator :: [Int] -> Bool
-- Return True if the sum of elements passed in list is divisible by 10.
mod10Validator xs = mod (sum xs) 10 == 0

cardValidator :: Int -> Bool
-- Return True if card number is correct.
cardValidator n = (mod10Validator (integerListToDigits (doubleUp (integerToDigits n) ) ) )

main :: IO ()
-------------------------------Valid card numbers-------------------------------
-- main = print(cardValidator 122000000000003)
-- main = print(cardValidator 378282246310005)
-- main = print(cardValidator 4222222222222)
-- main = print(cardValidator 5555555555558726)
-------------------------------Invalid card numbers-------------------------------
-- main = print (cardValidator 5555555555558736)
main = print (cardValidator 5555556555558736)

--------------------------------Functions testing---------------------------------
-- main = print (integerToDigits 123456)
-- main = print (integerListToDigits [123456])
-- main = print (doubleUp (integerListToDigits[123456789]))
-- main = print (mod10Validator(integerListToDigits[1234]))
-- main = print (mod10Validator(integerListToDigits[51815]))

-------------------Unused functions, kept as a code reference-------------------
returnEveryThirdElement :: [Int] -> [Int]
returnEveryThirdElement (x:y:z:xs) = y : returnEveryThirdElement xs
returnEveryThirdElement _ = []

returnEverySecondElement :: [Int] -> [Int]
returnEverySecondElement (x:y:xs) = y : returnEverySecondElement xs
returnEverySecondElement _ = []

sumNumbers :: [Int] -> Int
sumNumbers xs = sum xs
