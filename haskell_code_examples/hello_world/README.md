## Description
Hello world written in Haskell.

## Requirements
The only system package required to run this example is Nix package manager: \
https://nixos.org/download.html

To enter the nix shell:
```sh
nix-shell
```

## Used technologies
This example showcases the following technologies:
 - Nix package manager
   - Nix shell
 - Haskell
   - Stack

## Usage
Inside nix shell:

### Run GHCI interpreter
```sh
stack ghci
```

### Compile and run example file
```sh
stack ghc -- -o run_example example.hs
./ run_example
```

## Troubleshooting
> bash: ./run.sh: Permission denied

Solution: make script executable:
```sh
chmod +x run.sh
```
