class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


# 🗒️ 9b6783 TODO: add tests for Singleton
# 🗒️ 9b6783 TODO: add tests for Borg
class Borg:
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state
