from threading import Timer


class MyTimer:
    def __init__(self):
        self._seconds = 0
        self._minutes = 0
        self._hours = 0
        self.on_timeout = None
        self._timer = None

    @property
    def seconds(self):
        return self._seconds

    @seconds.setter
    def seconds(self, seconds):
        self._seconds = seconds

    @property
    def minutes(self):
        return self._minutes

    @minutes.setter
    def minutes(self, minutes):
        self._minutes = minutes

    @property
    def hours(self):
        return self._hours

    @hours.setter
    def hours(self, hours):
        self._hours = hours

    def get_remaining_time(self):
        pass

    def start(self):
        assert all(self._seconds > 0, self._minutes > 0, self._hours > 0)
        interval_seconds = self._seconds + 60 * self._minutes + 60 * 60 * self._hours
        self._timer = Timer(interval_seconds, self.on_timeout)
        self._timer.start()

    def cancel(self):
        self._timer.cancel()
