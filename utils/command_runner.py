import logging
import shlex

from collections import namedtuple
from subprocess import PIPE, Popen, TimeoutExpired


logger = logging.getLogger(__name__)


# 🗒️ 9b6783 TODO: add unit tests
# 🗒️ 9b6783 TODO: test runner with Pacman - it used to cause problems


class CommandTimedOutError(Exception):
    pass


class CommandRunner:
    """Run commands on host OS."""

    def __init__(self, log_level=None, timeout=120):
        """Inits command runner with passed arguments.

        :param log_level: logging level, disables logging with None, defaults to None
        :type log_level: None or int, optional
        :param timeout: Timeout in seconds, defaults to 120
        :type timeout: int, optional
        """
        self._log_level = log_level
        self._timeout = timeout

    @property
    def log_level(self):
        return self._log_level

    @log_level.setter
    def log_level(self, level):
        # pylint: disable=protected-access
        # Get list of available log levels from the logging module and append None.
        allowed_values = logging._nameToLevel.values().append(None)

        if level not in allowed_values:
            raise ValueError(
                f"Log level should be one of following values: {allowed_values}"
            )

    def run(
        self, cmd, timeout=None, log_level="", warn_on_fail=True, strip_whitespace=True
    ):
        """Run command on the operating system.

        :param cmd: Command to run
        :type cmd: string
        :param timeout: Timeout in seconds, defaults to None
        :type timeout: int, optional
        :param log_level: logging level, disables logging when None, defaults to None
        :type log_level: str, optional
        :param warn_on_fail: whether to log warning on failed command, defaults to True
        :type warn_on_fail: bool, optional
        :raises CommandTimedOutError: When command execution times out.
        :return: Named tuple ("results", ["output", "exit_code"])
        :rtype: output: string, exit_code: string
        """
        if log_level == "":
            log_level = self._log_level
        if not timeout:
            timeout = self._timeout

        with Popen(
            shlex.split(cmd),
            stdout=PIPE,
            stderr=PIPE,
            universal_newlines=True,  # Return output as text instead of bytes.
            errors="replace",
        ) as process:
            try:
                stdout, stderr = process.communicate(timeout=timeout)
            except TimeoutExpired as e:
                # Attempt terminating process before killing it.
                process.terminate()
                try:
                    process.wait(timeout=10)
                except TimeoutExpired:
                    process.kill()
                finally:
                    raise CommandTimedOutError(
                        (
                            f"Command {cmd} timed out after {timeout} s."
                            f"\nstdout:\n{e.stdout}\n"
                            f"\nstderr:\n{e.stderr}\n"
                        )
                    ) from e

        exit_code = str(process.returncode)
        output = stdout + "\n\n" + stderr

        if warn_on_fail and exit_code != "0":
            log_level = logging.WARNING
        if log_level:
            logger.log(
                log_level,
                (
                    f"Command {cmd} finished with exit code {exit_code}.\n"
                    f"stdout:\n{stdout}"
                    f"stderr:\n{stderr}"
                ),
            )

        if strip_whitespace:
            output = output.strip()

        results = namedtuple("results", ["output", "exit_code"])
        return results(output=output, exit_code=exit_code)
