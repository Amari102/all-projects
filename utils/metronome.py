import time

from pathlib import Path
from threading import Event, Thread

# pip install simpleaudio
import simpleaudio


# 🗒️ 9b6783 TODO: make compatible with mp3 and .ogg as well. Is there a library for this?
class Metronome:
    def __init__(self, soundfile_path, bpm):
        self.bpm = bpm
        self.soundfile_path = Path(soundfile_path).resolve()

        self._thread = None
        self._beat = simpleaudio.WaveObject.from_wave_file(str(self.soundfile_path))
        self._stopped = Event()

    def start_metronome(self):
        self._thread = Thread(target=self.metronome_loop)
        self._stopped.clear()
        self._thread.start()

    def stop_metronome(self):
        self._stopped.set()

    def metronome_loop(self):
        while not self._stopped:
            self._beat.play()
            time.sleep(60 / self.bpm)

    def change_tempo(self, bpm):
        # 🗒️ 9b6783 TODO: how to properly handle restarting/reloading thread?
        self._stopped.set()
        self.bpm = bpm
        self.stop_metronome()
        time.sleep(3)
        self.start_metronome()
