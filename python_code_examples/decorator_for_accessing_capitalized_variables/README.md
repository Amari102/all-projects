## Description
Problem: I had two data classes using identical variable names, except that one class had CAPITALIZED_NAMES and the other had lowercase_names. Unifying the syntax was impossible, as it would require changing too many files. It was neccessary to create a converter/decorator that would wrap around the class with capitalized names and convert calls to lowercase attributes to lowercase attributes.
## Usage
```sh
python3 example.py
```
