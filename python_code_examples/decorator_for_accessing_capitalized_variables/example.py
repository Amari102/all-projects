from dataclasses import dataclass


class CapitalizedData:
    FOO = "foo"
    BAR = "bar"
    HELLO_WORLD = "hello world"


@dataclass
class NormalData:
    foo: str = "foo"
    bar: str = "bar"
    hello_world: str = "hello_world"


class Wrapper:
    def __init__(self, base_obj, *args, **kwargs):
        self.base_obj = base_obj

    def __getattr__(self, name):
        try:
            return getattr(self.base_obj, name)
        except AttributeError:
            return getattr(self.base_obj, name.upper())


print(NormalData.hello_world)  # OK
# print(CapitalizedData.hello_world)  # AttributeError
print(Wrapper(CapitalizedData).hello_world)  # Fixed by using wrapper())
