with import <nixpkgs> {};
let
  inherit (pkgs) mkShell;
in mkShell {
  name = "poetry-example-env";
  buildInputs = with pkgs; [ poetry ];
}
