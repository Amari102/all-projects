## Description
Example showing how to set up simple development environment using Poetry and Nix.
Tutorial used: https://python-poetry.org/docs/basic-usage/

## Dependencies
Nix package manager: https://nixos.org/download.html
## Usage
First, enter nix shell to get access to **Poetry**. Then, use `poetry shell` to enter
the project environment:
```
nix-shell
cd poetry-demo/
# run poetry commands
poetry shell
```
### Create new project
```
poetry new my-awesome-project
```
### Add dependencies
```
poetry add Flask==2.2.3
poetry add httpie==3.2.1
```
### Install dependencies
```
poetry install
```

### Print path to virtualenv
```
poetry show -v | head -n 1
```
