import dataclasses
import json


@dataclasses.dataclass
class Recipe:
    recipe_id: int
    recipe_name: str
    recipe_description: str

class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)

recipes = [
    Recipe(1, "Fishcake", "Vossler Industries All-Natural Artificial-Fish-Derived Food Product"),
    Recipe(2, "Buffalo Steak Sandvich", "Who needs bread?"),
    Recipe(2, "Bunny Stew", "This one's luck has run out."),
]

if __name__ == "__main__":
    print(json.dumps(recipes, cls=EnhancedJSONEncoder))
