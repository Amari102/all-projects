import dataclasses
import json

from http import HTTPStatus

import flask


app = flask.Flask(__name__)


@dataclasses.dataclass
class Recipe:
    recipe_id: int
    recipe_name: str
    recipe_description: str

recipes = [
    Recipe(1, "Fishcake", "Vossler Industries All-Natural Artificial-Fish-Derived Food Product"),
    Recipe(2, "Buffalo Steak Sandvich", "Who needs bread?"),
    Recipe(3, "Bunny Stew", "This one's luck has run out."),
]

@app.route("/recipes", methods = ["GET"])
def get_recipes():
    return flask.jsonify(recipes)

@app.route("/recipes/<int:recipe_id>", methods = ["GET"])
def get_recipe(recipe_id):
    matching_recipe = next((r for r in recipes if r.recipe_id == recipe_id), None)
    if matching_recipe:
        return flask.jsonify(matching_recipe)
    return flask.jsonify({"message": "Recipe not found."}), HTTPStatus.NOT_FOUND


@app.route("/recipes", methods = ["POST"])
def create_recipe():
    # ️️️️📝 9b6783 NOTE: missing JSON validation here
    data = flask.request.get_json()

    recipe = Recipe(
        recipe_id = len(recipes) + 1,
        recipe_name = data.get("recipe_name"),
        recipe_description = data.get("recipe_description")
    )

    recipes.append(recipe)
    return flask.jsonify(recipe), HTTPStatus.CREATED

@app.route("/recipes/<int:recipe_id>", methods = ["PUT"])
def update_recipe(recipe_id):
    recipe = next((r for r in recipes if r.recipe_id == recipe_id), None)

    if not recipe:
        return flask.jsonify({"message": "Recipe not found."}), HTTPStatus.NOT_FOUND

    print(f"DEBUG 9b6783 {flask.request.data=}")
    data = flask.request.get_json()
    print(f"DEBUG 9b6783 {data=}")
    recipe.recipe_name = data.get("recipe_name")
    recipe.recipe_description = data.get("recipe_description")

    return flask.jsonify(recipe)

@app.route("/recipes/<int:recipe_id>", methods = ["DELETE"])
def delete_recipe(recipe_id):
    recipe = next((r for r in recipes if r.recipe_id == recipe_id), None)
    recipes.remove(recipe)
    return flask.jsonify({"Message": "Delete successful."}), HTTPStatus.NO_CONTENT

if __name__ == "__main__":
    app.run()
