## Description
This example shows simple CRUD API made with Flask.

This example showcases the following technologies:
 - Poetry for packaging
 - Nix-shell for providing Poetry
## Usage
```
nix-shell
poetry shell
python app.py
```
