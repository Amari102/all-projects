## Description
This example show simple "todo" style app that allows publishing recipes, built with \
Flask-RESTful.

This example showcases the following technologies:
 - Poetry for packaging
 - Nix-shell for providing Poetry
 - Flask-RESTful API
 - Integrated tests created with Pytest
## Usage
```
nix-shell
poetry shell
```

### Running app
```
PYTHONPATH=. python3 flask_api_example/app.py
```

### Running tests
```
python3 -m pytest -vvsx tests/
```
