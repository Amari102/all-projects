import dataclasses
import typing


recipe_list: typing.List["Recipe"] = []

def get_last_id():
    if recipe_list:
        last_recipe = recipe_list[-1]
    else:
        return 1
    return last_recipe.id + 1

@dataclasses.dataclass
class Recipe:
    name: str
    description: str
    num_of_servings: int
    cook_time: int
    directions: str
    is_publish: bool = False
    recipe_id: typing.Optional[int] = None

    def __post_init__(self):
        self.recipe_id = get_last_id()

    @property
    def data(self):
        return dataclasses.asdict(self)
