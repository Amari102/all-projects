import multiprocessing
import time

import pytest
import requests

from flask_api_example.app import run_app


@pytest.fixture(scope="function", name="app", autouse=True)
def fixture_app():
    p = multiprocessing.Process(target=run_app)
    p.start()
    time.sleep(0.01)  # sleep here to wait until server initializes.
    yield
    p.terminate()
    p.join()


@pytest.fixture(scope="function", name="server_url")
def fixture_server_url():
    return "http://127.0.0.1:5000"

class Tests:

    def test_recipes(self, server_url):
        """Test if /recipes endpoint always returns 200."""
        url = server_url + "/recipes"
        assert requests.get(url, timeout=10).status_code == 200

    def test_nonexistent_endpoint(self, server_url):
        url = server_url + "/wijwieufhbwieufhwieufb"
        assert requests.get(url, timeout=10).status_code == 404

    def test_recipe(self, server_url):
        url = server_url + "/recipe/1"
        assert requests.get(url, timeout=10).status_code == 404


