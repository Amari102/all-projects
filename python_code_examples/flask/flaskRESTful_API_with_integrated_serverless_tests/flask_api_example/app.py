import flask
import flask_restful

from flask_api_example.resources.recipe import (
    RecipeListResource,
    RecipePublishResource,
    RecipeResource,
)


def create_app():
    app = flask.Flask(__name__)
    api = flask_restful.Api(app)

    api.add_resource(RecipeListResource, "/recipes")
    api.add_resource(RecipeResource, "/recipe/<int:recipe_id>")
    api.add_resource(RecipePublishResource, "/recipes/<int:recipe_id>/publish")

    return app

def run_app():
    app = create_app()
    app.run(port=5000, debug=False)

if __name__ == "__main__":
    run_app()

