## Description
This example shows how to create simple API endpoint with Flask that prints \
"hello world".

## Description
This example showcases the following technologies:
 - Poetry for packaging
 - Nix-shell for providing Poetry
## Usage
```
nix-shell
poetry shell
python app.py
```
