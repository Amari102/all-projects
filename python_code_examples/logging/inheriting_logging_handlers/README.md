## Description
Adding loggers and handlers to the root logger also makes them available for
loggers created by `logging.getLogger(__name__)` in other files.
## Usage
```python
python -m my_module.example1
```
