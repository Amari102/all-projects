import logging


ROOT_LOGGER = logging.getLogger()
MODULE_LOGGER = logging.getLogger(__name__)


if __name__ == "__main__":
    print("Root logger handlers:")
    print(ROOT_LOGGER.handlers)
    print("Module logger handlers: ")
    print(MODULE_LOGGER.handlers)

    ROOT_LOGGER.debug("root logger debug message")
    ROOT_LOGGER.info("root logger info message")
    ROOT_LOGGER.warning("root logger warning message")
    ROOT_LOGGER.critical("root logger critical message")

    MODULE_LOGGER.debug("module logger debug message")
    MODULE_LOGGER.info("module logger info message")
    MODULE_LOGGER.warning("module logger warning message")
    MODULE_LOGGER.critical("module logger critical message")
