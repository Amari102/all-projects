import logging


logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger()
logger.setLevel("DEBUG")

spam_handler = logging.FileHandler("spam.log")
spam_handler.setLevel(logging.DEBUG)

important_handler = logging.FileHandler("important_messages.log")
important_handler.setLevel(logging.WARNING)

FORMAT = "custom formatter: %(message)s - %(levelname)s - %(name)s"
formatter = logging.Formatter(FORMAT)
spam_handler.setFormatter(
    logging.Formatter("<SPAM> %(message)s - %(levelname)s - %(name)s")
)
important_handler.setFormatter(
    logging.Formatter("<IMPORTANT> %(message)s - %(levelname)s - %(name)s")
)

logger.addHandler(spam_handler)
logger.addHandler(important_handler)
