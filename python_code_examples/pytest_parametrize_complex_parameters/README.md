## Description

**Problem**:
Parametrization statement is too complex.
```python
@pytest.mark.parametrize("writing_tool", [
    WritingTool(tool_type="pen", tool_color="blue"),
    WritingTool(tool_type="pencil", tool_color="gray"),
    WritingTool(tool_type="fountain pen", tool_color="pink"),
])
```

**Solution**:
Create separate classes and include them as parametrization parameters.
Bonus: improved pytest log output - instead of writing_tool0, writing_tool1, ..., class names are visible.
```
test.py::test_parametrized_1[writing_tool0] WritingTool(tool_type='pen', tool_color='blue')
PASSED
test.py::test_parametrized_1[writing_tool1] WritingTool(tool_type='pencil', tool_color='gray')
PASSED
test.py::test_parametrized_1[writing_tool2] WritingTool(tool_type='fountain pen', tool_color='pink')
PASSED
test.py::test_parametrized_2[Pen] <class 'pytest_parametrize_complex_parameters.test.Pen'>
PASSED
test.py::test_parametrized_2[Pencil] <class 'pytest_parametrize_complex_parameters.test.Pencil'>
PASSED
test.py::test_parametrized_2[FountainPen] <class 'pytest_parametrize_complex_parameters.test.FountainPen'>
PASSED
```

## Usage
```sh
python -um pytest -vvvvs test.py
```
