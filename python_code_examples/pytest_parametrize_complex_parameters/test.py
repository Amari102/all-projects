import random

from dataclasses import dataclass

import pytest


@dataclass
class WritingTool:
    tool_type: str
    tool_color: str


# Problem with this test: too large parametrize statement. Can it be split?
@pytest.mark.parametrize(
    "writing_tool",
    [
        WritingTool(tool_type="pen", tool_color="blue"),
        WritingTool(tool_type="pencil", tool_color="gray"),
        WritingTool(tool_type="fountain pen", tool_color="pink"),
    ],
)
def test_parametrized_1(writing_tool):
    print(writing_tool)


class Pen(WritingTool):
    tool_color = "blue"
    tool_type = "pen"
    line_thickness = random.uniform(0, 1)


class Pencil(WritingTool):
    tool_color = "gray"
    tool_type = "pencil"
    line_thickness = random.uniform(0, 1)


class FountainPen(WritingTool):
    tool_color = "pink"
    tool_type = "fountain pen"
    line_thickness = random.uniform(0, 1)


@pytest.mark.parametrize("writing_tool", [Pen, Pencil, FountainPen, Pen])
def test_parametrized_2(writing_tool):
    print(writing_tool.tool_color)
    # Note that line thickness is not recomputed for the second Pen object.
    # The values will not be recomputed in the subsequent tests.
    print(writing_tool.line_thickness)
