class MyCustomException(Exception):
    def __init__(self, msg, *args, **kwargs):
        if msg:
            msg = "!!! " + msg.upper() + " !!!"
            super().__init__(msg, *args, **kwargs)
        else:
            super().__init__(*args, **kwargs)


print("Program starts")
raise MyCustomException(msg="Houston, we have a problem")
# raise MyCustomException()
