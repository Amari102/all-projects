import dataclasses
import enum


class Arrow(enum.Enum):
    LEFT = "<-"
    RIGHT = "->"
    UP = "^"
    DOWN = "\/"


print([(item.name, item.value) for item in Arrow])
