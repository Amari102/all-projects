## Description
The purpose of this example is to get familiarized with usage of `TYPE_CHECKING`
feature of the typing library. This example also features example of generating
padded, centered strings which can be used for generating comments.
## Usage
```python
python test_type_checking.py
```
