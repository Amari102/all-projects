from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from typing import List

# Note: to generate a padded, centered string: '{:=^78}'.format(' Some text ')
# ================================= Example 1 =================================
# It's not possible to use types as a typing hint here, because error will be
# thrown during runtime.
# my_list1: List = ["a", "b"]
# NameError: name 'List' is not defined. Did you mean: 'list'?

# ================================= Example 2 =================================
# This example is not a good solution either, because it omits a type hint.
my_list2 = ["a", "b"]

# ================================= Example 3 =================================
# Enveloping the type in quotes allows mypy to perform type checking, while
# avoiding runtime errors. Note the warning thrown by mypy in line that tries
# to create a variable "my_list_invalid_type" which is a tuple instead of list.
my_list_correct_type: "List" = ["a", "b"]
my_list_invalid_type: "List" = ("a", "b")
