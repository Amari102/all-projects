from dataclasses import dataclass

import pytest


@dataclass
class Pen:
    ink_color: str = "blue"
    tool_type: str = "pen"


@dataclass
class Pencil:
    ink_color = "gray"
    tool_type = "pencil"


@pytest.mark.parametrize("writing_tool", [Pen, Pencil])
def test_parametrized(writing_tool):
    print("print: ", writing_tool)
    print("repr: ", repr(writing_tool))  # No difference.
