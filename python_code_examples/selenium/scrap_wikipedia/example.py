"""
The purpose of this test is to demonstrate simple example of extracting web data using
webdriver.
"""
import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.service import Service as FirefoxService


# Prevent opening any UI windows
os.environ["MOZ_HEADLESS"] = "1"

# Create a web driver instance using Gecko engine
driver = webdriver.Firefox()

# Visit the desired page
driver.get("https://en.wikipedia.org/wiki/Cycladic_art")

# Get the opening paragraph
# Note: xpaths differ in the Wikipedia skin used inside Selenium:
# /html/body/div/div[1]/div[1]/div[3]/div[4]/div[1]/p[1]  # correct xpath
# /html/body/div[1]/div/div[3]/main/div[3]/div[3]/div[1]/p[1]  # xpath in Selenium
# However, CSS selector is identical for Selenium version.
first_paragraph = driver.find_element(
    By.XPATH, "/html/body/div[1]/div/div[3]/main/div[3]/div[3]/div[1]/p[1]"
).text

second_paragraph = driver.find_element(
    By.CSS_SELECTOR, ".mw-parser-output > p:nth-child(7)"
).text

print(first_paragraph, "\n\n", second_paragraph)

# Close the web driver
driver.close()
