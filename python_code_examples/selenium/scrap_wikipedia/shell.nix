with import <nixpkgs> {};
let
  inherit (pkgs) mkShell;
  pythonEnv = python311.withPackages (ps: [ ps.selenium ]);
in mkShell {
  name = "spanishdict-sentence-extractor-env";
  buildInputs = with pkgs; [ pythonEnv geckodriver ];
}
