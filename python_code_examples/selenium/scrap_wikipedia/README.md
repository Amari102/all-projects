## Description
This example shows how to extract wikipedia paragraph text using Selenium webdriver.

Xpath and CSS selector can be retrieved using developer tools.

## Usage
```sh
nix-shell
python example.py
```
