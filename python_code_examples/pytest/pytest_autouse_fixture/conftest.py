import pytest


@pytest.fixture(autouse=True, scope="function")
def prepare_for_test():
    print("This is a setup function that must run before every test")
