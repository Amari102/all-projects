import time


def test_long_test():
    sleep_duration = 120
    print("Sleeping for {sleep_duration} s.")
    time.sleep(sleep_duration)
