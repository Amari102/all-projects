import functools


class WritingTool:
    def __init__(self, tool_type):
        self.tool_type = tool_type


class PrettyWrapper:
    def __init__(self, base_obj, *args, **kwargs):
        self.base_obj = base_obj

    def __str__(self):
        return "✨" + self.base_obj.__str__ + "✨"


class BetterPrettyWrapper:
    def __init__(self, base_obj, *args, **kwargs):
        self.base_obj = base_obj
        print(f"DEBUG 9b6783 {base_obj.__class__.__name__=}")
        # self.__type__ = str(base_obj.__class__).split(".")[-1]
        self.__type__ = base_obj.__class__.__name__
        print(f"DEBUG 9b6783 {self.__type__}")

    def __call__(self):
        print("in decorated __call__")
        return self.base_obj.__call__(self)

    @classmethod
    def __instancecheck__(cls, instance):
        # return isinstance(instance, User)
        return True

    def __str__(self):
        return "✨" + self.base_obj.__str__ + "✨"


fountain_pen = WritingTool("fountain pen")
print(f"{isinstance(fountain_pen, WritingTool)=}")

pencil = PrettyWrapper(WritingTool("pencil"))
print(f"{isinstance(pencil, WritingTool)=}")  # False - not the expected result

marker = BetterPrettyWrapper(WritingTool("marker"))
print(f"{isinstance(marker, WritingTool)=}")
