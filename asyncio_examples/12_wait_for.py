"""Run tasks in parallel."""

import asyncio
import random
import time


async def get_random_number():
    return 10 * random.random()


async def count():
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    random_number = await asyncio.shield(get_random_number())
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


async def main():
    try:
        await asyncio.wait_for(count(), 3)
    except TimeoutError:
        print("Task execution exceeded time limit of 3 seconds")
    else:
        print("Task execution took less than 3 seconds")

    try:
        await asyncio.shield(asyncio.wait_for(count(), 3))
    except TimeoutError:
        print("Task execution exceeded time limit of 3 seconds")
    else:
        print("Task execution took less than 3 seconds")


if __name__ == "__main__":
    s = time.perf_counter()
    asyncio.run(main())
    time.sleep(10)
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
