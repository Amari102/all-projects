"""Run tasks in parallel."""

import asyncio
import random
import time


async def count():
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    random_number = 10 * random.random()
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


async def main():
    try:
        async with asyncio.timeout(4):
            async with asyncio.TaskGroup() as tg:
                task1 = tg.create_task(count())
                # Task 2 is shielded - if main() is cancelled, count() will still run.
                task2 = asyncio.shield(tg.create_task(count()))
                task3 = tg.create_task(count())

                print(f"started at {time.strftime('%X')}")

            # The wait is implicit when the context manager exits.

            print(f"finished at {time.strftime('%X')}")
    except asyncio.TimeoutError:
        # Internally, TimeoutError is converted from CancelledError.
        # Do note that the shielded task is still cancelled.
        print("Execution timed out after 4 seconds, aborting.")


if __name__ == "__main__":
    s = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
