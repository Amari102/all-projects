### Build docker
```
./run_docker.sh
```
### Run examples
Minimum python version to run the examples is 3.11.
```
python3.11 01_run_tasks_sequentially.py
python3.11 05_task_groups.py
```
### Examples overview
Following examples were based on official asyncio documentation at https://docs.python.org/3.12/library/asyncio-task.html.

- 01: Run tasks sequentially
- 02: Run tasks in parallel using await keyword
- 03: Showcase error when tasks are created, but not properly run
- 04: Run tasks in parallel using asyncio.create_task
- 05: Showcase error when using TaskGroup but forgetting to use TaskGroup.create_task
- 06: Run tasks in parallel using TaskGroup
- 07: Get coroutine result by awaiting it
- 08: Get coroutine result by converting it into a task and awaiting it
- 09: Highlight proper usage of introduced asyncio features.
    - Always keep references to created tasks
        - Launch fire-and-forget tasks by adding them to set
    - Robustly perform clean-up logic in coroutines with try/finally block.
    - Add asyncio.sleep(0) during time-expensive operations to avoid blocking the main loop.
- 10: Showcase asyncio.shield
- 11: Showcase asyncio.timeout
- 12: Showcase asyncio.wait_for as well as my failure at understanding how shields work
- 13: Showcase asyncio.wait

----
Not covered in the examples:
- asyncio.as_completed
- asyncio.to_thread - This coroutine function is primarily intended to be used for executing IO-bound functions/methods that would otherwise block the event loop if they were run in the main thread.
- asyncio.run_coroutine_threadsafe - This function is meant to be called from a different OS thread than the one where the event loop is running.
- asyncio.current_task - Return the currently running Task instance, or None if no task is running.
- asyncio.all_tasks - Return a set of not yet finished Task objects run by the loop.
