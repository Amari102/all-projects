"""Run tasks in parallel."""

import asyncio
import random
import time


async def get_random_number():
    # It takes 5 second to generate random number.
    await asyncio.sleep(5)
    return 10 * random.random()


async def count():
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    # Create task to wait for random number to become available
    random_number_task = asyncio.create_task(get_random_number())
    random_number = await random_number_task
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


async def main():
    async with asyncio.TaskGroup() as tg:
        task1 = tg.create_task(count())
        task2 = tg.create_task(count())
        task3 = tg.create_task(count())

        print(f"started at {time.strftime('%X')}")

    # The wait is implicit when the context manager exits.

    print(f"finished at {time.strftime('%X')}")


if __name__ == "__main__":
    print(f"Program started at {time.strftime('%X')}.")
    s = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
