"""Run tasks in parallel."""

import asyncio
import random
import time


async def get_random_number():
    return 10 * random.random()


async def count():
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    random_number = await asyncio.shield(get_random_number())
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


async def main():
    # Wait for all tasks to complete
    tasks = [
        asyncio.create_task(count(), name="task 1"),
        asyncio.create_task(count(), name="task 2"),
        asyncio.create_task(count(), name="task 3"),
    ]
    done, pending = await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
    print("All tasks were completed")

    # Wait until first task is completed
    tasks = [
        asyncio.create_task(count(), name="task 1"),
        asyncio.create_task(count(), name="task 2"),
        asyncio.create_task(count(), name="task 3"),
    ]
    done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
    print("First task has finished executing")
    print(f"Done tasks: {[task.get_name() for task in done]}")
    print(f"Pending tasks: {[task.get_name() for task in pending]}")


if __name__ == "__main__":
    s = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
