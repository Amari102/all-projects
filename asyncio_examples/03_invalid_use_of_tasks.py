"""Run tasks in parallel."""

import asyncio
import random
import time


async def count():
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    random_number = 10 * random.random()
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


async def main():
    # These tasks start concurrently, but program ends immediately before they finish.
    task1 = asyncio.create_task(count())  # pylint: disable=unused-variable
    task2 = asyncio.create_task(count())  # pylint: disable=unused-variable


if __name__ == "__main__":
    s = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
