#!/bin/sh
CONTAINER_NAME=asyncio_examples_docker

# Stop previous container if it exists
if [ "$( docker container inspect -f '{{.State.Running}}' "$CONTAINER_NAME" )" == "true" ]; then
    docker stop "$CONTAINER_NAME"
    while docker container inspect "$CONTAINER_NAME" >/dev/null 2>&1; do sleep 1; done
fi

# Build container
docker build -t $CONTAINER_NAME . --progress=tty
# Start new container
docker run -it \
    -v $(pwd)/..:/home/asyncio_user \
    $CONTAINER_NAME \
    /usr/bin/fish
