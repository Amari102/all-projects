"""Run tasks in parallel."""

import asyncio
import random
import time


async def get_random_number():
    # It takes 5 second to generate random number.
    # Coroutines should have robust try/finally clauses to handle being cancelled.
    try:
        await asyncio.sleep(5)
        return 10 * random.random()
    except asyncio.CancelledError:  # If task was cancelled early
        print("Task get_random_number was cancelled")
        # do some cleanuo
        raise  # propagate the cancel exception - some asyncio components rely on it.
    finally:
        print("Doing cleanup in get_random_number")


async def count(*args, **kwargs):
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    print("count() started")
    # Create task to wait for random number to become available
    random_number_task = asyncio.create_task(get_random_number())
    random_number = await random_number_task
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


def long_running_code():
    time.sleep(1)


async def time_consuming_operation():
    # This function runs for a long time.
    # To avoid blocking main event loop, sleep for 0s in between execution.
    long_running_code()
    await asyncio.sleep(0)

    long_running_code()
    await asyncio.sleep(0)

    long_running_code()
    await asyncio.sleep(0)
    print("Time consuming operation is completed.")


async def main():
    async with asyncio.TaskGroup() as tg:
        # Save result of create_task to avoid tasks disappearing mid-execution:
        # tg.create_task(count())  # BAD
        # task = tg.create_task(count())  # BETTER
        task1 = tg.create_task(count())
        task2 = tg.create_task(time_consuming_operation())
        # TaskGroup can be passed to coroutines. New tasks can be added to the group,
        # after it started, but only as long as "async with" has not exited yet
        # (__aexit__ has not been called).
        task3 = tg.create_task(count(tg))

        # The event loop only keeps weak references to tasks. A task that isn’t
        # referenced elsewhere may get garbage collected at any time, even before it’s
        # done. For reliable “fire-and-forget” background tasks, gather them in
        # a collection:
        background_tasks = set()

        for _i in range(3):
            task = asyncio.create_task(count())

            # Add task to the set. This creates a strong reference.
            background_tasks.add(task)

            # To prevent keeping references to finished tasks forever,
            # make each task remove its own reference from the set after
            # completion:
            task.add_done_callback(background_tasks.discard)

        print(f"TaskGroup execution started at {time.strftime('%X')}")

    # The wait is implicit when the context manager exits.

    # loop.create_task or ensure_future is low-level alternative for asyncio.create_task

    print(f"finished at {time.strftime('%X')}")


if __name__ == "__main__":
    print(f"Program started at {time.strftime('%X')}.")
    s = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
