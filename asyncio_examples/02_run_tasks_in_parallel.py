"""Run tasks in parallel."""

import asyncio
import random
import time


async def count():
    """
    The count function prints a random number between 0 and 10, then sleeps for that
    many seconds.
    It is useful for illustrating asyncio.

    :return: The number of seconds it slept for
    :doc-author: Trelent
    """
    random_number = 10 * random.random()
    print(f"count started at {time.strftime('%X')}. sleeping for {random_number:0.2f}s")
    # Yield control to the event loop by using await keyword
    await asyncio.sleep(random_number)
    print("count end")


async def main():
    # asyncio.TaskGroup is more modern replacement for gather(). Added in 3.11
    # If any awaitable in aws is a coroutine, it is automatically scheduled as a Task.
    _results = await asyncio.gather(count(), count(), count())
    # If all awaitables are completed successfully, the result is an aggregate list of
    # returned values.


if __name__ == "__main__":
    s = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")

    assert elapsed > 0.2, "Program execution ended sooner than expected."
