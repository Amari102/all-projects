import importlib
import json
import logging

from typing import Type

from rhasspy_intent_handler_asyncio.abstract_classes import (
    AbstractIntentHandler,
)
from rhasspy_intent_handler_asyncio.intent_utils import BaseIntent


logger = logging.getLogger(__name__)


class IntentHandler(AbstractIntentHandler):
    """Receive intent messages from Rhasspy and delegate them to appropriate Intents."""

    def __init__(self):
        self.mqtt_client = None

    @property
    def client(self):
        return self.mqtt_client

    @client.setter
    def client(self, mqtt_client):
        self.mqtt_client = mqtt_client

    async def handle_intent(self, msg):
        """Find Intent matching received message and execute it."""
        nlu_payload = json.loads(msg.payload)
        intent_name = nlu_payload["intent"]["intentName"]
        # get class with correct name and execute action
        intent_class = self.get_intent_class_by_name(intent_name)
        intent = intent_class(
            nlu_payload=nlu_payload,
            intent_handler=self,
        )
        await intent.handle_intent()

    def get_intent_class_by_name(self, intent_name) -> Type[BaseIntent]:
        """Get Intent class from its string name."""
        module = importlib.import_module(
            f"rhasspy_intent_handler_asyncio.intents.{intent_name}"
        )
        return getattr(module, intent_name)
