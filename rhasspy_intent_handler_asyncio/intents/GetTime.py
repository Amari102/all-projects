import time

import inflect

from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    UsesTTSMixin,
)


class GetTime(BaseIntent, UsesTTSMixin):
    """
    time_unit = (date|time){time_unit}
    get <time_unit>
    current <time_unit>
    what's the <time_unit>
    """

    async def handle_intent(self):
        """Announce current time via text to speech."""
        time_unit = self.slots.get("time_unit")

        if time_unit == "time":
            time_unit_message = time.strftime("%H, %M")

        elif time_unit == "date":
            # Strftime cheatsheet: https://strftime.org/
            day_of_week = time.strftime("%A")
            inflector = inflect.engine()
            day_number = time.strftime("%-d")
            day = inflector.number_to_words(inflector.ordinal(day_number))
            month = time.strftime("%B")
            week = time.strftime("%W")

            time_unit_message = f"Today is {day_of_week}, {day} of {month}, week {week}"
        else:
            assert False, f"Unexpected time unit: {time_unit}."

        await self.text2speech(time_unit_message)
