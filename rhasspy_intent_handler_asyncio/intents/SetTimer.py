import asyncio

from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    UsesTTSMixin,
)


class SetTimer(BaseIntent, UsesTTSMixin):
    """
    hours = (1){hour} hour | (2..6){hour} hours
    minutes = (1){min} minute | (2..60){min} minutes
    set [a] timer (for|to) <minutes>
    set [a] timer (for|to) <hours>
    set [a] timer (for|to) <hours> and <minutes>
    """

    # 🗒️ 9b6783 TODO: add intent for checking timer status.
    # 🗒️ 9b6783 TODO: add stopwatch intent.

    async def handle_intent(self):
        """Set timer for X hours and Y minutes."""
        slots = self.nlu_payload["slots"]
        seconds_to_wait = 0
        for slot in slots:
            if slot["slotName"] == "hour":
                hours = slot["value"]["value"]
                seconds_to_wait += 60 * 60 * hours
            if slot["slotName"] == "min":
                minutes = slot["value"]["value"]
                seconds_to_wait += 60 * minutes

        await self.text2speech(f"Timer set to {int(seconds_to_wait/60)} minutes.")

        # Add warning when there are 2 minutes remaining
        if seconds_to_wait > (10 * 60):
            await asyncio.sleep(seconds_to_wait - 120)
            await self.text2speech("Two minutes remaining.")
            await asyncio.sleep(120)
        else:
            await asyncio.sleep(seconds_to_wait)

        await self.text2speech(msg="Beep! Beep! Beep! Time's out! Beep! Beep! Beep!")
