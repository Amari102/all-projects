from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    CanPublishMixin,
)
from rhasspy_intent_handler_asyncio.my_devices import (
    AllLights,
    DeskLights,
    NightLight,
    TradfriBulb,
)


class SetLightsAttributes(BaseIntent, CanPublishMixin):
    """
    light_group_name = (all lights | desk lights | night light) {light_group_name}
    attribute_to_change = (brightness | temperature) {attribute_to_change}
    (set | change) <light_group_name> <attribute_to_change> to (0..9){value}
    """

    async def handle_intent(self):
        """Change light parameters (brightness and temperature) of specified group."""

        # 🗒️ 9b6783 TODO: allow controlling individual bulbs
        # 🗒️ 9b6783 TODO: add alias for turning off all lights

        attribute_value = self.slots["value"]
        light_group_name = self.slots["light_group_name"]
        attribute_to_change = self.slots["attribute_to_change"]

        if attribute_to_change == "brightness":
            payload = {
                "brightness": TradfriBulb.get_brightness_value_by_level(
                    level=attribute_value
                )
            }
        else:
            payload = {
                "color_temp": TradfriBulb.get_temperature_value_by_level(
                    level=attribute_value
                )
            }

        # Select light group to change attributes in.
        if light_group_name == "all lights":
            light_group = AllLights()
        elif light_group_name == "night light":
            light_group = NightLight()
        else:
            light_group = DeskLights()

        for device in light_group.devices:
            await self.publish(topic=device.topic, payload=payload)
