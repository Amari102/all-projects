import asyncio

from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    CanRunCommandsMixin,
)


class OpenNotebookPage(BaseIntent, CanRunCommandsMixin):
    """
    page_name = (work scratchpad | scratchpad){page_name}
    open notebook page [(with | containing)] <page_name>
    open <page_name>
    """

    async def handle_intent(self):
        """Open Zim notebook page with given name."""
        notebook_name = "NOTES"
        pages = {
            "work scratchpad": "3 WORK:000 scratchpad",
            "scratchpad": "000 scratchpad",
        }

        page_name = self.slots.get("page_name")

        await self.run_command(f'zim {notebook_name} "{pages[page_name]}"')
        await asyncio.sleep(0.3)
        await self.run_command("i3-msg [urgent=latest] focus")
