from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    CanRunCommandsMixin,
)


class ToggleMusic(BaseIntent, CanRunCommandsMixin):
    """
    toggle music
    """

    async def handle_intent(self):
        """Switch to urgent i3 workspace."""
        await self.run_command(cmd="playerctl play-pause")
