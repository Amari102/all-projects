from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    CanRunCommandsMixin,
)


# 🗒️ 9b6783 TODO: merge with ToggleMusic
class MusicNext(BaseIntent, CanRunCommandsMixin):
    """
    next [song]
    """

    async def handle_intent(self):
        """Switch to urgent i3 workspace."""
        await self.run_command(cmd="playerctl next")
