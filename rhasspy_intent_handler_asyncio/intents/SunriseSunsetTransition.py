import asyncio

from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    CanPublishMixin,
    UsesTTSMixin,
)
from rhasspy_intent_handler_asyncio.my_devices import (
    DeskLights,
    NightLight,
    TradfriBulb,
)


class SunriseSunsetTransition(BaseIntent, CanPublishMixin, UsesTTSMixin):
    """
    transition_type = (sunrise|sunset) {transition_type}
    optional_duration = [with duration of (0..60){duration} minutes]
    optional_delay = [(with|and) delay of (0..12){initial_delay} hours]

    simulate <transition_type> <optional_duration> [and] <optional_delay>
    start <transition_type> simulation <optional_duration> [and] <optional_delay>
    load program <transition_type> simulation <optional_duration> [and] <optional_delay>
    """

    async def handle_intent(self):
        """Gradually adjust lights brightness and temperature.

        Sunrise simulation gradually increases night light brightness.
        Sunset simulation gradually decreases desk lights brightness.
        """

        # ️️️️📝 9b6783 NOTE: what if lights turned on one by one?
        # ️️️️📝 9b6783 NOTE: minute[s] and hour[s] are used frequently in the rhasspy
        # sentences. More robust logic handling them could be introduced, allowing
        # intents to reference simpler <time> variable, to enable specifying time with
        # sentences such as "one hour and 30 minutes", "one minute", "half a minute".

        duration = self.slots.get("duration", 60)
        duration_s = 60 * duration  # seconds

        initial_delay = self.slots.get("initial_delay", 0)

        # 9b6783 NOTE: update interval could be decreased for shorter simulations (<10m)
        update_interval_sec = 30

        # 9b6783 NOTE: syntax could look better if self.slots was a named tuple:
        # if self.slots.transition_type...
        # can namedtuple gracefully handle accessing non-existent val? (e.g return None)
        transition_type = self.slots.get("transition_type")

        status_message = (
            f"Starting {transition_type} transition with duration of {duration} minutes"
        )
        if initial_delay != 0:
            status_message += f", and delay of {initial_delay} hours."
        await self.text2speech(msg=status_message)

        if transition_type == "sunrise":
            initial_brightness = TradfriBulb.BRIGHTNESS_MIN
            final_brightness = TradfriBulb.BRIGHTNESS_MAX
            initial_temp = TradfriBulb.TEMP_MAX
            final_temp = TradfriBulb.TEMP_MIN
            device_group = NightLight()
        else:
            initial_brightness = TradfriBulb.BRIGHTNESS_MAX
            final_brightness = TradfriBulb.BRIGHTNESS_MIN
            initial_temp = TradfriBulb.TEMP_MIN
            final_temp = TradfriBulb.TEMP_MAX
            device_group = DeskLights()

        num_steps = int((duration_s / update_interval_sec))

        brightness_step = (final_brightness - initial_brightness) / num_steps
        temp_step = (final_temp - initial_temp) / num_steps

        current_brightness = initial_brightness
        current_temp = initial_temp

        await asyncio.sleep(initial_delay * 60 * 60)

        for _step in range(num_steps):
            current_brightness += brightness_step
            current_temp += temp_step
            await asyncio.sleep(update_interval_sec)

            for device in device_group.devices:
                # 🗒️ 9b6783 TODO: change light parameters concurrently
                await self.set_light_parameters(
                    topic=device.topic, brightness=current_brightness, temp=current_temp
                )

    async def set_light_parameters(self, topic: str, brightness: float, temp: float):
        """Send command to change brightness and color temperature to the topic."""
        payload = {
            "color_temp": int(temp),
            "brightness": int(brightness),
        }
        await self.publish(topic, payload)
