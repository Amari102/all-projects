import time

from rhasspy_intent_handler_asyncio.intent_utils import BaseIntent


class Nevermind(BaseIntent):
    """
    nevermind
    never mind
    """

    async def handle_intent(self):
        """Do not execute any action."""
