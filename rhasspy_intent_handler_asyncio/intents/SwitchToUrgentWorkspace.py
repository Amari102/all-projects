from rhasspy_intent_handler_asyncio.intent_utils import (
    BaseIntent,
    CanRunCommandsMixin,
)


class SwitchToUrgentWorkspace(BaseIntent, CanRunCommandsMixin):
    """
    on screen
    """

    async def handle_intent(self):
        """Switch to urgent i3 workspace."""
        await self.run_command("i3-msg [urgent=latest] focus")
