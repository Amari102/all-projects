import asyncio
import logging
import signal
import sys

from collections import namedtuple
from pathlib import Path

from yaml import safe_load

from rhasspy_intent_handler_asyncio.intent_handler import IntentHandler
from rhasspy_intent_handler_asyncio.mqtt_client import MQTTClient


try:
    import coloredlogs

except ImportError:
    pass

logger = logging.getLogger(__name__)

log_level: int = logging.DEBUG


def init_logging():
    """Set up loggers."""

    if "coloredlogs" in sys.modules:
        coloredlogs_level = logging.getLevelName(log_level)
        coloredlogs.install(level=coloredlogs_level)


def get_configuration(init_file_path):
    """Read the project configuration from YAML file."""
    # 🗒️ 9b6783 TODO: make docker-compose.yml read this file as well.
    init_file_path = Path(init_file_path)

    with open(init_file_path, mode="r", encoding="utf-8") as config_file:
        yaml_data = safe_load(config_file)
        mqtt_address = yaml_data["mqtt_server_address"]
        mqtt_port = yaml_data["mqtt_server_port"]

    Configuration = namedtuple(
        "Configuration", ["mqtt_server_address", "mqtt_server_port"]
    )
    return Configuration(mqtt_server_address=mqtt_address, mqtt_server_port=mqtt_port)


def handle_exit(*_args, **_kwargs):
    """Gracefully exit program."""
    print("\n")
    logger.info("Exiting.")
    sys.exit(0)


init_logging()
configuration = get_configuration(
    init_file_path=Path(".") / "rhasspy_intent_handler_asyncio/config.yml"
)

mqtt_client = MQTTClient(
    intent_handler=IntentHandler(),
    server_address=configuration.mqtt_server_address,
    server_port=configuration.mqtt_server_port,
)

signal.signal(signal.SIGTERM, handle_exit)
signal.signal(signal.SIGINT, handle_exit)

asyncio.run(mqtt_client.main())
