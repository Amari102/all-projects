"""Utility functions for creating new intents."""
from abc import ABC
from dataclasses import dataclass
from typing import Any, Dict, NamedTuple, Protocol

from rhasspy_intent_handler_asyncio.abstract_classes import (
    AbstractIntentHandler,
)
from utils.command_runner import CommandRunner


@dataclass
class AbstractIntent(ABC):
    """Abstract class representing Rhasspy intent."""

    nlu_payload: Dict[str, Any]
    intent_handler: AbstractIntentHandler

    async def handle_intent(self):
        """Execute action when intent arrives."""


class BaseIntent(AbstractIntent):
    """Base class for creating intents.

    IMPORTANT: Every intent should have docstring containing sentences that will be
    passed to Rhasspy for command recognition.
    """

    def __init__(self, intent_handler, nlu_payload):
        super().__init__(
            intent_handler=intent_handler,
            nlu_payload=nlu_payload,
        )
        self.slots = self.extract_slots_from_nlu_payload(nlu_payload)

    def extract_slots_from_nlu_payload(self, payload) -> Dict:
        """Extract slots from NLU payload and add them to dictionary for easy access."""

        slots = payload["slots"]
        slotnames_values = {}
        for slot in slots:
            slotnames_values[slot["slotName"]] = slot["value"]["value"]
        return slotnames_values


# ======================================================================================


class UsesTTS(Protocol):
    """Protocol for using text to speech."""

    async def text2speech(self) -> None:
        ...


class CanRunCommands(Protocol):
    """Protocol for running operating system commands."""

    async def run_command(self, cmd) -> NamedTuple:
        ...


class CanPublish(Protocol):
    """Protocol for publishing MQTT messages."""

    async def publish(self, topic, payload) -> None:
        ...


# ======================================================================================


class CanRunCommandsMixin(CanRunCommands):
    async def run_command(self, cmd) -> NamedTuple:
        runner = CommandRunner()
        return runner.run(cmd=cmd)


# 🗒️ 9b6783 TODO: fix linter warnings
class UsesTTSMixin(UsesTTS):
    """Mixin implementing Rhasspy's text to speech engine."""

    async def text2speech(self, msg):
        # 🗒️ 9b6783 TODO: fix linter warnings
        assert (
            self.intent_handler is not None
        ), "Class using this mixin is required to have intent_handler attribute."
        await self.intent_handler.mqtt_client.publish_tts_msg(msg)


class CanPublishMixin(CanPublish):
    """Mixin implementing sending messages to MQTT server."""

    async def publish(self, topic, payload):
        # 🗒️ 9b6783 TODO: fix linter warnings
        assert (
            self.intent_handler is not None
        ), "Class using this mixin is required to have intent_handler attribute."
        await self.intent_handler.mqtt_client.publish(topic, payload)
