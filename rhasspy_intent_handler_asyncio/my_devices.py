from dataclasses import dataclass


@dataclass
class TradfriBulb:
    name: str
    topic: str
    BRIGHTNESS_MIN: int = 0
    BRIGHTNESS_MAX: int = 255
    TEMP_MIN: int = 250
    TEMP_MAX: int = 454

    def __hash__(self) -> int:
        return hash(self.name)

    @staticmethod
    def get_brightness_value_by_level(level, max_level=9):
        return int(
            TradfriBulb.BRIGHTNESS_MIN
            + (TradfriBulb.BRIGHTNESS_MAX / max_level) * level
        )

    @staticmethod
    def get_temperature_value_by_level(level, max_level=9):
        return int(TradfriBulb.TEMP_MIN + (TradfriBulb.TEMP_MAX / max_level) * level)


@dataclass
class DeskLight1(TradfriBulb):
    def __init__(self):
        self.name = "desk_light_1"
        super().__init__(name=self.name, topic=f"zigbee2mqtt/{self.name}/set")


@dataclass
class DeskLight2(TradfriBulb):
    def __init__(self):
        self.name = "desk_light_2"
        super().__init__(name=self.name, topic=f"zigbee2mqtt/{self.name}/set")


@dataclass
class DeskLight3(TradfriBulb):
    def __init__(self):
        self.name = "desk_light_3"
        super().__init__(name=self.name, topic=f"zigbee2mqtt/{self.name}/set")


@dataclass
class DeskLight4(TradfriBulb):
    def __init__(self):
        self.name = "desk_light_4"
        super().__init__(name=self.name, topic=f"zigbee2mqtt/{self.name}/set")


@dataclass
class DeskLight5(TradfriBulb):
    def __init__(self):
        self.name = "desk_light_5"
        super().__init__(name=self.name, topic=f"zigbee2mqtt/{self.name}/set")


@dataclass
class DeskLights:
    id = "desk_lights"

    def __init__(self):
        self.devices = [DeskLight4(), DeskLight5()]


@dataclass
class NightLight:
    id = "night_light"

    def __init__(self):
        self.devices = [DeskLight1()]


@dataclass
class AllLights:
    id = "all_lights"

    def __init__(self):
        self.devices = [
            DeskLight1(),
            DeskLight2(),
            DeskLight3(),
            DeskLight4(),
            DeskLight5(),
        ]
