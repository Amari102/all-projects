# Usage
Create configuration files according to https://www.zigbee2mqtt.io/guide/getting-started/ and set up the docker:
```sh
docker-compose up
```
Configuration files structure:
```
docker-compose.yml
mosquitto-data/
README.md
rhasspy-data/
zigbee2mqtt-data/
```
