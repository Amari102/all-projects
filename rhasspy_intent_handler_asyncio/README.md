# Usage
Set up necessary services and make adjustments to configuration if necessary (see docker/README.md). Once the services are running, run the following command to handle voice intents:
```bash
cd all-projects && pip install -r requirements.txt && python -m rhasspy_intent_handler_asyncio
```

Make sure the code is adjusted to Rhasspy settings.

Default configuration includes external MQTT broker and Zigbee2Mqtt service for communication with IoT devices. However, this project requires only rhasspy service to work. If you want to skip using external MQTT or Zigbee2Mqtt:
 - Disable those services in docker-compose.yml.
 - Configure Rhasspy to use __internal__ MQTT broker.

## Recommended Rhasspy configuration
> **_NOTE:_** Familiarising yourself with https://rhasspy.readthedocs.io/en/latest/tutorials/#getting-started-guide is advised before proceeding with following steps.

Rhasspy interface is available at http://localhost:12101/. Zigbee2Mqtt interface is available at http://localhost:8080/.

 - **MQTT**: External, mqtt:1883
 - **Audio Recording**: arecord, device=pulse
 - **Wake Word**: Porcupine
 - **Speech to Text**: either DeepSpeech or Pocketsphinx (Kaldi tends to execute random intents on sentences not present in ini file)
 - **Intent Recognition**: Fsticuffs
 - **Text to Speech**: Larynx (comparison of available voices is here: https://rhasspy.github.io/larynx/#en-us_blizzard_fls-glow_tts)
 - **Audio Playing**: aplay, device=pulse
 - **Dialogue Management**: Rhasspy
 - **Intent Handling**: disabled


### Generating Rhasspy sentences
Rhasspy sentences for voice command recognition can be generated from source code using the *rhasspy_sentence_generator* tool. Usage:
```sh
cd all-projects
python3.11 rhasspy_intent_handler_asyncio/rhasspy_sentence_generator.py
```
File will be generated under name *automatically_generated_sentences.ini*. At the moment of writing, the only way to use the newly generated sentences is to copy and paste them into sentences GUI window and retrain Rhasspy manually.

## Intent handling
Intent handling is done by *rhasspy_intent_handler_asyncio*. To start it, execute the following command:
```bash
cd all-projects && python -m rhasspy_intent_handler_asyncio
```

Functions linked to intents from *sentences.ini* are located in intents/ and their classes must be named ```intent<IntentName>```.

## Adding new intent
> **_NOTE:_** Guide on adding new sentences: https://www.youtube.com/watch?v=sWVl0ZoXZEo

### Example sentence
Sentence list is available in rhasspy configuration directory under *sentences.ini* file.
```
[GetTime]
current time
tell me the time
what's the time
```
Additional sentence files can be added in the GUI. This project uses additional file *automatically_generated_sentences.ini*.

### Example intent function
Take a look at ***GetTime.py*** to see how a minimal example intent is structured.

### Optional requirements
Install optional requirements to enable colored output and other features:
```bash
cd all-projects && pip install -r optional-requirements.txt
```

