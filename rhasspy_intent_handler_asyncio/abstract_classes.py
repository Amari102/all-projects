from abc import ABC, abstractmethod


class AbstractIntentHandler(ABC):
    """Abstract class for intents."""

    @property
    @abstractmethod
    def client(self):
        ...

    @client.setter
    @abstractmethod
    def client(self, mqtt_client):
        ...

    @abstractmethod
    async def handle_intent(self, msg):
        pass


class AbstractMQTTClient(ABC):
    """Abstract class for the MQTT client."""

    @abstractmethod
    async def handle_message(self, msg):
        pass

    async def handle_unrecognized_intent(self):
        pass

    async def publish_tts_msg(self, msg):
        pass

    async def publish(self, topic, payload):
        pass
