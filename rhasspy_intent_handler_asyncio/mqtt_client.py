# pip install paho-mqtt
import asyncio
import json
import logging
import typing

import asyncio_mqtt as aiomqtt

from rhasspy_intent_handler_asyncio.abstract_classes import (
    AbstractIntentHandler,
    AbstractMQTTClient,
)
from rhasspy_intent_handler_asyncio.intent_utils import CanRunCommandsMixin


logger = logging.getLogger(__name__)


# 🗒️ 9b6783 TODO: add authorisation
# 🗒️ 9b6783 TODO: add metronome
# 🗒️ 9b6783 TODO: implement cancelling tasks
# 🗒️ 9b6783 TODO: add support for radiator valve (Tuya):
# 🗒️ 9b6783 TODO: add pylintrc, ignore long lines starting with http
# 🗒️ 9b6783 TODO: add scripts that run the linters
class MQTTClient(AbstractMQTTClient, CanRunCommandsMixin):
    def __init__(self, intent_handler, server_address, server_port):
        self.mqtt_client: typing.Optional[aiomqtt.Client] = None
        self.intent_handler: AbstractIntentHandler = intent_handler
        self.intent_handler.client = self

        self.server_address = server_address
        self.server_port = server_port

        self.site_id = "default"
        self.music_status = ""

        # 🗒️ 9b6783 TODO: store tasks in a separate, serializeble? State class
        self.tasks = set()

    async def handle_message(self, msg):
        """Handle MQTT message.

        Check the topic of the message to see if its recognized as a valid intent, and
        then handle it with intent handler.
        """
        # 🗒️ 9b6783 TODO: refactor needed

        if str(msg.topic) == "hermes/dialogueManager/sessionStarted":
            await self.handle_wake_action(msg)

        elif str(msg.topic) == "hermes/nlu/intentNotRecognized":
            await self.handle_unrecognized_intent()

        else:
            await self.handle_intent(msg)

    async def handle_wake_action(self, msg):
        self.music_status = await self.run_command("playerctl status")
        self.music_status = self.music_status.output
        if self.music_status == "Playing":
            await self.run_command("playerctl pause")

    async def handle_session_end_action(self, msg):
        if self.music_status == "Playing":
            await self.run_command("playerctl play")

    async def handle_intent(self, msg):
        assert self.intent_handler is not None

        self.schedule_task(asyncio.create_task(self.intent_handler.handle_intent(msg)))
        await self.handle_session_end_action(msg)

    # 🗒️ 9b6783 TODO: this function should be reusable over the whole program.
    def schedule_task(self, task: asyncio.Task):
        """Schedule task and ensure it is not garbage collected until it's completed."""
        self.tasks.add(task)
        task.add_done_callback(self.tasks.discard)

    async def handle_unrecognized_intent(self):
        await self.publish_tts_msg(msg="Request denied!")

    async def publish_tts_msg(self, msg):
        """Publish message to be spoken by text-to-speech engine."""
        await self.publish("hermes/tts/say", {"text": msg, "siteId": self.site_id})

    async def main(self):
        self.mqtt_client = aiomqtt.Client("localhost")
        async with self.mqtt_client as client:
            async with client.messages() as messages:
                await client.subscribe("hermes/intent/#")
                await client.subscribe("hermes/nlu/intentNotRecognized")
                await client.subscribe("hermes/dialogueManager/sessionStarted")
                logger.info("Connected. Waiting for intents.")

                async for message in messages:
                    logger.info(f"Received message {message.topic}.")
                    self.schedule_task(
                        asyncio.create_task(self.handle_message(msg=message))
                    )

    async def publish(self, topic, payload):
        assert self.mqtt_client is not None
        payload_string = json.dumps(payload)
        logger.debug(f"Publishing message {payload_string} to topic {topic}.")
        await self.mqtt_client.publish(topic, payload_string)
