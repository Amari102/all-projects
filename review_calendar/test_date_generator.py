import dateutil.parser
import pytest

from review_calendar.review_date_generator import generate_review_dates


def test_generating_correct_dates_1():
    date = dateutil.parser.parse("1928-11-03")
    expected_review_dates = [
        "1928-Nov-05",
        "1928-Nov-07",
        "1928-Nov-11",
        "1928-Nov-19",
        "1928-Dec-05",
        "1929-Jan-06",
        "1929-Mar-11",
    ]
    assert generate_review_dates(date) == expected_review_dates
