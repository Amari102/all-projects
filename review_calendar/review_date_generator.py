import calendar

from datetime import datetime, timedelta

import dateutil.parser
import pytest


def generate_review_dates(date):
    review_intervals = [2, 4, 8, 16, 32, 64, 128]
    review_dates = []

    for interval in review_intervals:
        review_date = date + timedelta(days=interval)
        review_dates.append(review_date.strftime("%Y-%b-%d"))

    return review_dates


if __name__ == "__main__":
    input_date = input("Enter your date in ISO format. Default date: today.\n>_  ")

    if not input_date:
        date = datetime.now()
    else:
        date = dateutil.parser.parse(input_date)

    print(generate_review_dates(date))
