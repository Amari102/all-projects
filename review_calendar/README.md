# Review calendar

Generate recurring reminders for reviewing learning notes.

By default, the following schedule is used:
Reminder after 2, 4, 8, 16, 32, 64 and 128 days.

Current version is a work in progress and only generates the review dates based on input day.

## Usage
```sh
python3 review_calendar/review_date_generator.py
```
## Running tests
```sh
python3 -um pytest -vv review_calendar/
```
