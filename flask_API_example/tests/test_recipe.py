import pytest

from flask_api_example.app import create_app
from werkzeug.test import Client


@pytest.fixture(scope="function", name="app", autouse=True)
def fixture_app():
    app = create_app()
    c = Client(app)
    return c

class Tests:

    def test_recipes(self, app):
        response = app.get("/recipes")
        assert response.status_code == 200

    def test_nonexistent_endpoint(self, app):
        response = app.get("/wijwieufhbwieufhwieufb")
        assert response.status_code == 404
        print(response.__dict__)

    def test_recipe(self, app):
        response = app.get("/recipe/1")
        assert response.status_code == 404

    def test_create_recipe(self, app):
        example_recipe = {
            "name": "Peach Sangria",
            "description": "Life's a peach",
            "num_of_servings": "99999999",
            "cook_time": "-1",
            "directions": "Chop the peaches and mix them with wine",
        }
        response = app.post("/recipes", json=example_recipe)
        assert response.status_code == 200
        expected_response = {
            'name': 'Peach Sangria',
            'description': "Life's a peach",
            'num_of_servings': '99999999',
            'cook_time': '-1', 'directions':
            'Chop the peaches and mix them with wine',
            'is_publish': False,
            'recipe_id': 1
        }
        assert response.json == expected_response




