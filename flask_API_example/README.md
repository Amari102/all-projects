## Description
This example showcases the following technologies:
 - Poetry for packaging
 - Nix-shell for providing Poetry
 - Flask-RESTful API
 - Integrated tests created with Pytest
 - Werkzeug test client for fast, serverless testing

## Usage
```
nix-shell
poetry shell
```
### Starting Postgres database server
```
nix-shell postgresql.nix
```
See `postgresql.nix` for more details.

### Running app
```
PYTHONPATH=. python3 flask_api_example/app.py
```

### Running tests
```
python3 -m pytest -vvsx tests/
```
