let
nixpkgs = import (builtins.fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-22.11.tar.gz) {
  overlays = [];
  config = {};
};

in
with nixpkgs;

stdenv.mkDerivation {
  name = "flask-api-example-env";
  buildInputs = [];

  nativeBuildInputs = [
    fish
    fzf
    starship
    emacs-nox

    pgadmin4
    geos
    gdal
    nixpkgs-fmt

    glibcLocales  # Fix for initdb: error: invalid locale name

    # postgres-12 with postgis support
    (postgresql_12.withPackages (p: [ p.postgis ]))

    poetry
    python311
  ];
}
