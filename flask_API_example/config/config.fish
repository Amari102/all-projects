starship init fish | source

function fish_user_key_bindings
	fzf_key_bindings
end

# Ctrl - f,e,h,g is the sequence used for copying
bind \cf begin-selection
bind \cg end-selection
bind \ch fish_clipboard_copy

# Fix for nix-shell
set --export NIX_PATH $HOME/.nix-defexpr/channels
