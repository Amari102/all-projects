class Config:
    DEBUG = True
    name = "your_name"
    password = "your_name"
    db_name = "smilecook"
    SQLALCHEMY_DATABASE_URI = f'postgresql+psycopg2://{name}:{password}@localhost:5555/{db_name}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
