import http

import flask
import flask_restful

from flask_api_example.models.recipe import Recipe, recipe_list


def _find_recipe(recipe_id):
    return next((r for r in recipe_list if r.recipe_id == recipe_id), None)

def _recipe_not_found_error():
    return {"message": "Recipe not found."}, http.HTTPStatus.NOT_FOUND

class RecipeListResource(flask_restful.Resource):
    def get(self):
        data = []

        for recipe in recipe_list:
            if recipe.is_publish is True:
                data.append(recipe.data)

        return {"data": data}, http.HTTPStatus.OK

    def post(self):
        data = flask.request.get_json()

        recipe = Recipe(
            name = data["name"],
            description=data["description"],
            num_of_servings = data["num_of_servings"],
            cook_time = data["cook_time"],
            directions = data["directions"],
        )

        recipe_list.append(recipe)

        return recipe.data, http.HTTPStatus.OK


class RecipeResource(flask_restful.Resource):
    def get(self, recipe_id):
        recipe = _find_recipe(recipe_id)
        if not recipe:
            return _recipe_not_found_error()
        return recipe.data, http.HTTPStatus.OK

    def put(self, recipe_id):
        data = flask.request.get_json()

        recipe = _find_recipe(recipe_id)
        if not recipe:
            return _recipe_not_found_error()

        recipe.name = data["name"]
        recipe.description = data["description"]
        recipe.num_of_servings = data["num_of_servings"]
        recipe.cook_time = data["cook_time"]
        recipe.directions = data["directions"]

        return recipe.data, http.HTTPStatus.OK

class RecipePublishResource(flask_restful.Resource):
    def put(self, recipe_id):
        # Note: PUT method here is not necessarily used for update.
        recipe = _find_recipe(recipe_id)
        if not recipe:
            return _recipe_not_found_error()

        recipe.is_publish = True
        return {}, http.HTTPStatus.NO_CONTENT

    def delete(self, recipe_id):
        # Note: DELETE method here is not necessarily used for delete.
        recipe = _find_recipe(recipe_id)
        if not recipe:
            return _recipe_not_found_error()

        recipe.is_publish = False
        return {}, http.HTTPStatus.NO_CONTENT
